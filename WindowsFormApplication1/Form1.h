#pragma once
#using<system.dll>

namespace WindowsFormApplication1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			
			Int16 dobra = 0;
			Int16 zla = 0;
			bool var = false;
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::RadioButton^  radioButton1;
	private: System::Windows::Forms::RadioButton^  radioButton3;
	private: System::Windows::Forms::RadioButton^  radioButton4;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::RadioButton^  radioButton2;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: StreamReader^ din;
	private: Int16 dobra;
	private: Int16 zla;
	private: String^ odpowiedz;
	private: bool var;
	private: System::Windows::Forms::Button^  NowaGraButton;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::GroupBox^  groupBox1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->radioButton1 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton3 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton4 = (gcnew System::Windows::Forms::RadioButton());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->radioButton2 = (gcnew System::Windows::Forms::RadioButton());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->NowaGraButton = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox2->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// radioButton1
			// 
			this->radioButton1->AutoSize = true;
			this->radioButton1->Font = (gcnew System::Drawing::Font(L"Palatino Linotype", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->radioButton1->Location = System::Drawing::Point(16, 11);
			this->radioButton1->Name = L"radioButton1";
			this->radioButton1->Size = System::Drawing::Size(14, 13);
			this->radioButton1->TabIndex = 4;
			this->radioButton1->TabStop = true;
			this->radioButton1->UseVisualStyleBackColor = true;
			// 
			// radioButton3
			// 
			this->radioButton3->AutoSize = true;
			this->radioButton3->Font = (gcnew System::Drawing::Font(L"Palatino Linotype", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->radioButton3->Location = System::Drawing::Point(276, 11);
			this->radioButton3->Name = L"radioButton3";
			this->radioButton3->Size = System::Drawing::Size(14, 13);
			this->radioButton3->TabIndex = 6;
			this->radioButton3->TabStop = true;
			this->radioButton3->UseVisualStyleBackColor = true;
			// 
			// radioButton4
			// 
			this->radioButton4->AutoSize = true;
			this->radioButton4->Font = (gcnew System::Drawing::Font(L"Palatino Linotype", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->radioButton4->Location = System::Drawing::Point(276, 46);
			this->radioButton4->Name = L"radioButton4";
			this->radioButton4->Size = System::Drawing::Size(14, 13);
			this->radioButton4->TabIndex = 7;
			this->radioButton4->TabStop = true;
			this->radioButton4->UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->button1->Location = System::Drawing::Point(231, 330);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(84, 62);
			this->button1->TabIndex = 12;
			this->button1->Text = L"Dalej";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::Color::Firebrick;
			this->button2->Location = System::Drawing::Point(352, 330);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(85, 62);
			this->button2->TabIndex = 13;
			this->button2->Text = L"Zako�cz";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->label5->Font = (gcnew System::Drawing::Font(L"Nokia Standard Multiscript", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(238)));
			this->label5->Location = System::Drawing::Point(32, 98);
			this->label5->MaximumSize = System::Drawing::Size(500, 100);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(0, 29);
			this->label5->TabIndex = 14;
			// 
			// radioButton2
			// 
			this->radioButton2->AutoSize = true;
			this->radioButton2->Font = (gcnew System::Drawing::Font(L"Palatino Linotype", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->radioButton2->Location = System::Drawing::Point(16, 46);
			this->radioButton2->Name = L"radioButton2";
			this->radioButton2->Size = System::Drawing::Size(14, 13);
			this->radioButton2->TabIndex = 5;
			this->radioButton2->TabStop = true;
			this->radioButton2->UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this->groupBox2->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->groupBox2->Controls->Add(this->radioButton4);
			this->groupBox2->Controls->Add(this->radioButton3);
			this->groupBox2->Controls->Add(this->radioButton2);
			this->groupBox2->Controls->Add(this->radioButton1);
			this->groupBox2->Location = System::Drawing::Point(37, 193);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(552, 75);
			this->groupBox2->TabIndex = 16;
			this->groupBox2->TabStop = false;
			// 
			// NowaGraButton
			// 
			this->NowaGraButton->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(255)), 
				static_cast<System::Int32>(static_cast<System::Byte>(128)));
			this->NowaGraButton->Location = System::Drawing::Point(477, 330);
			this->NowaGraButton->Name = L"NowaGraButton";
			this->NowaGraButton->Size = System::Drawing::Size(85, 62);
			this->NowaGraButton->TabIndex = 17;
			this->NowaGraButton->Text = L"Nowy test";
			this->NowaGraButton->UseVisualStyleBackColor = false;
			this->NowaGraButton->Click += gcnew System::EventHandler(this, &Form1::NowaGraButton_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->label1->Font = (gcnew System::Drawing::Font(L"MS Reference Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->label1->Location = System::Drawing::Point(6, 16);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(179, 16);
			this->label1->TabIndex = 18;
			this->label1->Text = L"Dobrych odpowiedzi: 0";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->label3->Font = (gcnew System::Drawing::Font(L"Nokia Standard Multiscript", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->label3->Location = System::Drawing::Point(396, 43);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(92, 29);
			this->label3->TabIndex = 20;
			this->label3->Text = L"Kategoria:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->label2->Font = (gcnew System::Drawing::Font(L"MS Reference Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->label2->Location = System::Drawing::Point(6, 48);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(159, 16);
			this->label2->TabIndex = 19;
			this->label2->Text = L"Zlych odpowiedzi: 0";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->label4->Font = (gcnew System::Drawing::Font(L"MS Reference Sans Serif", 9.75F, System::Drawing::FontStyle::Bold));
			this->label4->Location = System::Drawing::Point(6, 79);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(156, 16);
			this->label4->TabIndex = 21;
			this->label4->Text = L"Pytan do konca: 15";
			// 
			// groupBox1
			// 
			this->groupBox1->BackColor = System::Drawing::SystemColors::ScrollBar;
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(5, 305);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(190, 124);
			this->groupBox1->TabIndex = 22;
			this->groupBox1->TabStop = false;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::ControlLight;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"$this.BackgroundImage")));
			this->ClientSize = System::Drawing::Size(640, 480);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->NowaGraButton);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Name = L"Form1";
			this->Text = L"TEST WIEDZY";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) 
		 {
		 }
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 Application::Exit();
		 }
private: System::Void NowaGraButton_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			var = true;
			label1->Text = "Dobrych odpowiedzi: 0";
			label2->Text = "Zlych odpowiedzi: 0";
			din = File::OpenText("pytania.txt");
			label5->Text =(din->ReadLine());
			radioButton1->Text = (din->ReadLine());
			radioButton2->Text = (din->ReadLine());
			radioButton3->Text = (din->ReadLine());
			radioButton4->Text = (din->ReadLine());
			odpowiedz = (din->ReadLine());
			label3->Text = "Kategoria:"+(din->ReadLine());
			label4->Text = "Pytan do konca: 15";
			zla = 0;
			dobra = 0;
		 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) 
		 {	
			 String^ sign;
			 if(var==false)
			 {
				 MessageBox::Show("Zacznij now� gr�!!!");
			 }
			 else if(var==true)
			 {				
				if(radioButton1->Checked) {sign = "A";}
				else if(radioButton2->Checked) {sign = "B";}
				else if(radioButton3->Checked) {sign = "C";}
				else if(radioButton4->Checked) {sign = "D";}
				
				if (sign == odpowiedz)
					{
						dobra++;
						label1->Text = "Dobrych odpowiedzi: " + dobra.ToString();
						label4->Text = "Pytan do konca: "+((15-(dobra+zla)).ToString());
						MessageBox::Show("Dobra odpowied�!!!");				
					}
				else if(sign != odpowiedz) 
					{
						zla++;
						label2->Text = "Zlych odpowiedzi: " + zla.ToString();
						label4->Text = "Pytan do konca: "+((15-(dobra+zla)).ToString());
						MessageBox::Show("Z�a dopowied�!!! Dobra odpowiedz to: "+odpowiedz);				
					}
				if(din->EndOfStream==false)
					{
						label5->Text =(din->ReadLine());
						radioButton1->Text = (din->ReadLine());
						radioButton2->Text = (din->ReadLine());
						radioButton3->Text = (din->ReadLine());
						radioButton4->Text = (din->ReadLine());
						odpowiedz = (din->ReadLine());
						label3->Text = "Kategoria: "+(din->ReadLine());
						
					}
				else if(din->EndOfStream==true)
					{
						MessageBox::Show("Koniec. Udzieli�e� poprawnych odpowiedzi: "+dobra+" na 15");
						var = false;
					}		 		
					radioButton1->Checked = false;
					radioButton2->Checked = false;
					radioButton3->Checked = false;
					radioButton4->Checked = false;
			}
		 }
};
}